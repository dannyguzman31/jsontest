import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Iterator;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class JsonTest {

    public static void main(String[] args) throws Exception {
    /*
        Write Json to a file JsonTest.json
     */
        // Create a Json Object
        JSONObject obj = new JSONObject();

        obj.put("firstName", "Daniel");
        obj.put("lastName", "Guzman");
        obj.put("age", 29);
        obj.put("EmployeeId", 123456);

        // Use a Map for address Data
        Map map = new LinkedHashMap();
        map.put("streetAddress", "3031 Santa Monica Blvd");
        map.put("city", "Santa Monica");
        map.put("state", "California");
        map.put("postCode", 90404);

        obj.put("address", map); // put map into object

        // Write data to Json File
        PrintWriter printWriter = new PrintWriter("JsonTest.json");
        printWriter.write(obj.toJSONString());

        printWriter.flush();
        printWriter.close(); // close document

        System.out.println("JSON Object: ");
        System.out.println(obj);

        /*
            Reads from JsonTest.json
         */
        System.out.println();
        System.out.println("Read from JsonTest File");
        // Parse file "JsonTest.json"
        Object newObj = new JSONParser().parse(new FileReader("JsonTest.json"));
        // typecast object
        JSONObject ouput = (JSONObject) newObj;
        // get data from file
        String firstName = (String) ouput.get("firstName");
        String lastName = (String) ouput.get("lastName");

        System.out.println(firstName);
        System.out.println(lastName);

        long age = (long) ouput.get("age");
        System.out.println("Age: " + age);

        long id = (long) ouput.get("EmployeeId");
        System.out.println("Employee ID: " + id);

        Map address = ((Map)ouput.get("address"));

        Iterator<Map.Entry> itr1 = address.entrySet().iterator();
        while (itr1.hasNext()) {
            Map.Entry pair = itr1.next();
            System.out.println(pair.getKey() + " : " + pair.getValue());

        }
    }
}
